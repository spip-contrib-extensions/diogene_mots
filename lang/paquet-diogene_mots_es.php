<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_mots?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_mots_description' => 'Complemento palabras clave para "Diógenes"',
	'diogene_mots_nom' => 'Diógenes - Palabras',
	'diogene_mots_slogan' => 'Complemento palabras clave para "Diógenes"'
);
