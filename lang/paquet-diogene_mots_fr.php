<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_mots.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_mots_description' => 'Complément mots-clés pour "Diogene"',
	'diogene_mots_nom' => 'Diogene - Mots',
	'diogene_mots_slogan' => 'Complément mots-clés pour "Diogene"'
);
