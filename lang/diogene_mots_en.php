<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_mots?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_confirmer_creation_mots_nouveaux' => 'Confirm the creation in this group?',
	'erreur_mot_dans_autre_groupe' => 'Keyword @mot@ already exists in group @groupe@. ',

	// F
	'form_legend' => 'Keyword selector',
	'form_legend_public' => 'Keywords',

	// L
	'label_montrer_titre_et_descriptif' => 'When it exists, show the keyword description between brackets', # MODIF
	'label_mots_creer_dans_public' => 'Allow for creation of new keywords in these groups',
	'label_mots_facultatifs' => 'Optional keywords groups',
	'label_mots_obligatoires' => 'Required keyword groups'
);
