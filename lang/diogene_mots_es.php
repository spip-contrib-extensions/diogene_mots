<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_mots?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_confirmer_creation_mots_nouveaux' => '¿Confirmar la creación en este grupo?',
	'erreur_mot_dans_autre_groupe' => 'La palabra-clave @mot@ ya existe en el grupo @groupe@. ',

	// F
	'form_legend' => 'Selector de palabras clave',
	'form_legend_public' => 'Palabras clave',

	// L
	'label_montrer_titre_et_descriptif' => 'Descripción',
	'label_montrer_titre_et_descriptif_case' => 'Si existe, mostrar la descripción de la palabra entre paréntesis.',
	'label_mots_creer_dans_public' => 'Permitir la creación de nuevas palabras-clave en estos grupos',
	'label_mots_facultatifs' => 'Grupos de palabras facultativas',
	'label_mots_obligatoires' => 'Grupos de palabras obligatorias'
);
