<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_mots?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_mots_description' => 'Keywords add-on for "Diogene"',
	'diogene_mots_nom' => 'Diogene - Keywords',
	'diogene_mots_slogan' => 'Keywords add-on for "Diogene"'
);
