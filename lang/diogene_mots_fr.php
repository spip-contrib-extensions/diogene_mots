<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_mots.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_confirmer_creation_mots_nouveaux' => 'Confirmer tout de même la création dans ce groupe ?',
	'erreur_mot_dans_autre_groupe' => 'Le mot @mot@ existe déjà dans le groupe @groupe@. ',

	// F
	'form_legend' => 'Sélecteur de mots-clés',
	'form_legend_public' => 'Mots-clés',

	// L
	'label_montrer_titre_et_descriptif' => 'Descriptif',
	'label_montrer_titre_et_descriptif_case' => 'S’il existe, montrer le descriptif du mot entre parenthèses',
	'label_mots_creer_dans_public' => 'Permettre la création de nouveaux mots dans ces groupes',
	'label_mots_facultatifs' => 'Groupes de mots facultatifs',
	'label_mots_obligatoires' => 'Groupes de mots obligatoires',
	'limiter_usage_mots' => 'Limiter à une liste de mots pour le groupe « @groupe@ »'
);
